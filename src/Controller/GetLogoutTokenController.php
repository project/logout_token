<?php
namespace Drupal\logout_token\Controller;

use Drupal\user\Controller\UserAuthenticationController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides controllers for logout token.
 */
class GetLogoutTokenController extends UserAuthenticationController {

  /**
   * Generate Logout token.
   */
  public function getToken() {
    $logout_route = $this->routeProvider->getRouteByName('user.logout.http');
    // Trim '/' off path to match \Drupal\Core\Access\CsrfAccessCheck.
    $logout_path = ltrim($logout_route->getPath(), '/');
    return new Response($this->csrfToken->get($logout_path), 200, ['Content-Type' => 'text/plain']);
  }
}

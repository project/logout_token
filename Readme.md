# Logout Token Module

## Description

The Logout Token module extends Drupal 8, 9, and 10 core functionality by providing an additional endpoint that allows developers to retrieve the logout token during user sessions. The Logout Token module enhances Drupal core by providing an endpoint that allows developers to obtain the logout token during the initial user login through the user login endpoint. Unlike the core behavior, this module ensures that developers can retrieve the logout token at any time during the user session. This capability becomes particularly useful in scenarios where users reset their passwords but are unable to log out due to the logout token being regenerated alongside it. In such cases, the module's endpoint becomes a valuable tool for obtaining the user's logout token and handling secure logout operations effectively.

## Requirements

- Drupal 8, 9, or 10
- User module (version 8.x-1.0 or higher)

## Installation

Follow the standard installation procedure


## How to Use

Once the module is enabled, you can use the following endpoint to obtain the logout token:
/session/logout/token

This endpoint returns the logout token for the current user session, which can be useful in various scenarios where secure logout handling is required.

